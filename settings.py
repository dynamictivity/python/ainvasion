class Settings():
    """A class to store all settings for Alien Invasion."""

    def __init__(self):
        """Initialize the game's settings."""
        # Screen settings
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (230, 230, 230)

        # How quickly the game speeds up
        self.speedup_scale = 1.1

        # How quickly the alien point values increase
        self.score_scale = 1.5

        # Hardcore mode disabled by default
        self.hardcore_mode = False
        self.hardcore_bullet_width = 6

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """Initialize settings that may change throughout the game."""
        # Ship settings
        self.ship_limit = 2
        self.ship_speed_factor = 1.5

        # Bullet settings
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = 60, 60, 60
        self.bullets_allowed = 3
        self.bullet_collide_kill = True
        self.bullet_max_speed = 10
        self.bullet_speed_factor = 3

        # Alien settings
        self.alien_width = 60
        self.alien_height = 58
        self.alien_size_scale = .99
        self.alien_speed_factor = 1

        # Fleet settings
        self.fleet_drop_speed = 10
        # fleet_direction of 1 represents right; -1 represents left.
        self.fleet_direction = 1

        # Scoring
        self.alien_points = 50

    def increase_speed(self):
        """Increase speed settings and alien point values."""
        self.ship_speed_factor *= self.speedup_scale
        if self.bullet_speed_factor < self.bullet_max_speed or not self.hardcore_mode:
            self.bullet_speed_factor *= self.speedup_scale
        self.alien_speed_factor *= self.speedup_scale
        self.alien_points = int(self.alien_points * self.score_scale)