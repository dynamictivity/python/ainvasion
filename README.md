Alien Invasion
--------------
This game is the result of a tutorial in the book "Python Crash Course".

## Setup dev Environment
1. `brew install hg sdl sdl_image sdl_ttf sdl_mixer portmidi`
1. Install `python3` and `pip`
1. Setup a `virtualenv` and activate it -- https://packaging.python.org/guides/installing-using-pip-and-virtualenv/
    1. `python3 -m virtualenv env`
    1. `source env/bin/activate`
1. `pip install -r requirements.txt`

## Playing the game
1. `pip3 install -r requirements.txt`
1. `python3 alien_invasion.py`
